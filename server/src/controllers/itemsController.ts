import { Request, Response } from 'express';
import knex from '../database/connection';
import base_url from '../config/serverconfig';

class ItemsControllers {
  async index(request: Request, response: Response) {
    const items = await knex('items').select('*');
  
    const serializedItems = items.map(item => {
        return {
          id: item.id,
          title: item.title,
          image_url: `${base_url.link}/uploads/${item.image}`,
        }
    });
  
    return response.json(serializedItems);
  }
}

export default ItemsControllers;