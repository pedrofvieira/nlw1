import axios from 'axios';

import base_url from '../serverconfig.json';

const api = axios.create({
  baseURL: base_url.link
});

export default api;